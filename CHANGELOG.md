# Changelog

All notable changes to this project will be documented in this file. See
[standard-version](https://github.com/conventional-changelog/standard-version)
for commit guidelines.

## 0.0.0 (2022-02-09)

### Chore

- initial commit
  ([8f40527](https://gitlab.com/andrea_me/heroesapp/-/commit/8f405279723f8d09ff1da381df2062802b1b18f9))
